package core;

import network.AS;
import network.Topology;
import relation.StrictRelation;
import utility.Pair;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class PeerUtility
{
    public static final HashSet<Long> T1 = new HashSet<>(Arrays.asList(
            286L, 3356L, 5511L, 1299L, 2914L, 3257L, 6762L, 6453L, 3491L, 3549L,
            4323L, 6461L, 209L, 12956L, 3320L, 7018L, 701L, 2828L, 6830L, 1239L));

    public static boolean isT2T(Long A, Long B)
    {
        return T1.contains(A) && T1.contains(B);
    }

    public static boolean isT1(Long A)
    {
        return T1.contains(A);
    }

    /**
     * Retrieves the first Roof pair from the list.
     * Whatever is found first (X -> Y):
     * - When X is a T1 AS
     * - When X -> Y is P2C
     * - When X -> Y is P2P
     */
    public static Pair<AS, AS> getRoof(Topology topology, List<Pair<Long, Long>> pairs)
    {
        for(Pair<Long, Long> pair : pairs)
        {
            AS ASa = topology.ASes.get(pair.getFirst());
            AS ASb = topology.ASes.get(pair.getSecond());

            if(PeerUtility.isT1(pair.getFirst()))
                return new Pair<>(ASa, ASb);

            if(ASa.relations.containsKey(ASb.ASN))
            {
                StrictRelation R = ASa.relations.get(ASb.ASN);
                if(R.type == StrictRelation.Type.P2C || R.type == StrictRelation.Type.P2P)
                    return new Pair<>(ASa, ASb);
            }
        }

        return null;
    }

    public static boolean onlyOneT1(List<Pair<Long, Long>> pairs)
    {
        boolean T1 = false;
        boolean end = false;
        for(Pair<Long, Long> pair : pairs)
        {
            if(end && (isT1(pair.getFirst()) || isT1(pair.getSecond())))
                return false;

            if(T1 && (!isT1(pair.getFirst()) || !isT1(pair.getSecond())))
            {
                T1 = false;
                end = true;
            }

            if(!T1 && (isT1(pair.getFirst()) || isT1(pair.getSecond())))
                T1 = true;
        }

//        boolean found = false;
//        for(Pair<Long, Long> pair : pairs)
//        {
//            if(!found && isT1(pair.getFirst()) && !isT1(pair.getSecond()))
//                found = true;
//        }

        return true;
    }
}

package core;

public class Relationship implements Comparable<Relationship>
{
    public enum Type
    {
        PROVIDER_CUSTOMER,
        CUSTOMER_PROVIDER,
        PEER_PEER,
        SIBLING_SIBLING,
        FULL_TRANSIT,
        NONE;

        public static Type getType(int value)
        {
            switch (value)
            {
                case -1: return PROVIDER_CUSTOMER;
                case 0: return PEER_PEER;
                case 1: return CUSTOMER_PROVIDER;
                case 2: return PEER_PEER;
                case 3: return SIBLING_SIBLING;
                case 4: return FULL_TRANSIT;
                case 255: return NONE;
                default: return null;
            }
        }

        public static Type fromString(String value)
        {
            switch (value)
            {
                case "P2C": return PROVIDER_CUSTOMER;
                case "P2P": return PEER_PEER;
                case "C2P": return CUSTOMER_PROVIDER;
                case "S2S": return SIBLING_SIBLING;
                case "T2T": return FULL_TRANSIT;
                case "NONE": return NONE;
                default: return null;
            }
        }

        public Type invert()
        {
            if(this == PROVIDER_CUSTOMER)
                return CUSTOMER_PROVIDER;
            else if(this == CUSTOMER_PROVIDER)
                return PROVIDER_CUSTOMER;
            else
                return this;
        }
    }

    public final AS from;
    public final AS to;
    public final Type type;
    public final String source;

    public Relationship(AS from, AS to, Type type, String source)
    {
        this.from = from;
        this.to = to;
        this.type = type;
        this.source = source;
    }

    public void print()
    {
        System.out.println(from.ASN + " --> " + type.toString() + " --> " + to.ASN + " (" + source + ")");
    }

    @Override
    public int compareTo(Relationship r)
    {
        if(this.from.ASN == r.from.ASN && this.to.ASN == r.to.ASN && this.type == r.type && this.source.equals(r.source))
            return 0;
        else
            return -1;
    }
}

package core;

import java.util.HashSet;
import java.util.Set;

public class AS implements Comparable<AS>
{
    public final long ASN;
    public String owner;

    public final Set<Relationship> relationships = new HashSet<>();

    public AS(long ASN)
    {
        this.ASN = ASN;
        this.owner = "";
    }
    public AS(long ASN, String owner)
    {
        this.ASN = ASN;
        this.owner = owner;
    }

    public void addRelation(Relationship r)
    {
        if(r.from.ASN == ASN)
            relationships.add(new Relationship(r.from, r.to, r.type, r.source));
        else if(r.to.ASN == ASN)
            relationships.add(new Relationship(r.to, r.from, r.type.invert(), r.source));
    }

    public Relationship.Type getRelation(long ASN)
    {
        for(Relationship r : relationships)
            if(r.to.ASN == ASN)
            {
                if(!owner.isEmpty() && owner.equals(r.to.owner))
                    return Relationship.Type.SIBLING_SIBLING;
                return r.type;
            }

        return Relationship.Type.NONE;
    }

    @Override
    public int compareTo(AS a)
    {
        if(this.ASN == a.ASN)
            return 0;
        else
            return -1;
    }
}

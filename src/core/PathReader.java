package core;

import bgp.BGPReader;
import bgp.PathAttribute;
import bgp.attributes.AS_PATH;
import bgp.utility.BGPUtility;
import mrt.MRTReader;
import mrt.MRTRecord;
import mrt.type.TABLE_DUMP;
import mrt.type.TABLE_DUMP_V2.RIB_AFI;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import utility.Pair;

import java.io.*;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.zip.GZIPInputStream;

public class PathReader
{
    private final MRTReader reader;
    private Iterator<RIB_AFI.RIB_ENTRY> iterator = null;

    public PathReader(File file) throws Exception
    {
        System.out.println("Scraping: " + file.getName());

        DataInputStream stream;
        if(file.getName().endsWith(".gz"))
            stream = new DataInputStream(new GZIPInputStream(new FileInputStream(file)));
        else if(file.getName().endsWith(".bz2"))
            stream = new DataInputStream(new BZip2CompressorInputStream(new FileInputStream(file)));
        else
            stream = new DataInputStream(new FileInputStream(file));

        reader = new MRTReader(stream);
    }

    public AS_PATH getNextSanitizedPath() throws Exception
    {
        if(iterator != null)
            if(iterator.hasNext())
            {
                RIB_AFI.RIB_ENTRY next = iterator.next();
                AS_PATH path = BGPUtility.getAS_PATH(new DataInputStream(new ByteArrayInputStream(next.attributes)));
                BGPUtility.sanitize(path);

//                // TODO: DEFINITLY REMOVE THIS.
//                List<Pair<Long, Long>> pairs = BGPUtility.getPairs(path);
//                Collections.reverse(pairs);
//                for (Pair<Long, Long> pair : pairs)
//                    if((pair.getFirst().equals(262671L) || pair.getFirst().equals(28347L) || pair.getFirst().equals(263444L)) &&
//                            (pair.getSecond().equals(262671L) || pair.getSecond().equals(28347L) || pair.getSecond().equals(263444L)))
//                    {
//                        DataInputStream stream = new DataInputStream(new ByteArrayInputStream(next.attributes));
//                        BGPReader.Settings settings = new BGPReader.Settings();
//                        settings.EXTENDED_AS4 = true;
//                        settings.MRTMode = false;
//                        PathAttribute attr = PathAttribute.fromStream(stream, settings);
//                        try
//                        {
//                            while (true)
//                            {
//                                attr.print();
//                                attr = PathAttribute.fromStream(stream, settings);
//                            }
//                        }
//                        catch (Exception e)
//                        {
//                            System.out.println("------------------------------------------------------------");
//                        }
//                    }

                return path;
            }
            else
                iterator = null;

        try
        {
            while (true)
            {
                MRTRecord entry = reader.read();

                if(!entry.header.type.toString().equals("TABLE_DUMP_V2"))
                    System.out.println(entry.header.type.toString());

                if (entry instanceof RIB_AFI)
                {
                    RIB_AFI a = (RIB_AFI) entry;
                    iterator = a.entries.iterator();
                    AS_PATH path = BGPUtility.getAS_PATH(new DataInputStream(new ByteArrayInputStream(iterator.next().attributes)));
                    BGPUtility.sanitize(path);
                    return path;
                }
            }
        } catch (EOFException e)
        {
            System.out.println("[" + new Date().toString() + "] End of stream found!");
            return null;
        }
    }
}

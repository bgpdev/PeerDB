package core;

import providers.ASRank;
import providers.CustomNetwork;
import providers.RSEFNetwork;
import relation.Relation;
import relation.StrictRelation;

import java.time.Instant;
import java.util.*;

public class Network
{
    private HashMap<Long, AS> map = new HashMap<>();

    private static Network network = null;

    public Network(Instant time) throws Exception
    {

//        System.out.println("Network: Loading Routing Registry relationships ...");
//        for(Map.Entry<Long, AS> x : RPSL.getASNs(date).entrySet())
//        {
//            if(!map.containsKey(x.getKey()))
//                map.put(x.getKey(), new AS(x.getKey()));
//        }

//        System.out.println("Network: Loading custom relationships ...");
//        for (Relationship r : CustomNetwork.getRelationships(time, map))
//        {
//            if(!map.containsKey(r.from.ASN))
//                map.put(r.from.ASN, new AS(r.from.ASN));
//            if(!map.containsKey(r.to.ASN))
//                map.put(r.to.ASN, new AS(r.to.ASN));
//
//            AS from = map.get(r.from.ASN);
//            from.addRelation(r);
//            AS to = map.get(r.to.ASN);
//            to.addRelation(r);
//        }
//        System.out.println("Network: Done!");

        System.out.println("Network: Loading ASRank relationships ...");
        for (Relationship r : ASRank.getRelationships(time, map))
        {
            if(!map.containsKey(r.from.ASN))
                map.put(r.from.ASN, new AS(r.from.ASN));
            if(!map.containsKey(r.to.ASN))
                map.put(r.to.ASN, new AS(r.to.ASN));

            AS from = map.get(r.from.ASN);
            from.addRelation(r);
            AS to = map.get(r.to.ASN);
            to.addRelation(r);
        }
        System.out.println("Network: Done!");

//        System.out.println("Network: Loading RSEF relationships ...");
//        for(AS x : RSEFNetwork.getASNs(time))
//        {
//            if(!map.containsKey(x.ASN))
//                map.put(x.ASN, x);
//        }
//        System.out.println("Network: Done!");

    }

    public Relationship.Type getRelation(long AS1, long AS2)
    {
        if(!map.containsKey(AS1))
            return Relationship.Type.NONE;
        else
            return map.get(AS1).getRelation(AS2);
    }

    public Set<Relationship> getRelations(long AS)
    {
        if(!map.containsKey(AS))
            return new HashSet<>();
        else
            return map.get(AS).relationships;
    }

    public Set<Long> getCustomers(long AS, short limit)
    {
        Set<Long> customers = new HashSet<>();
        Queue<Long> left = new LinkedList<>();
        left.add(AS);

        while(!left.isEmpty())
        {
            long X = left.remove();
            customers.add(X);

            for(Relationship r : getRelations(X))
                if(r.type == Relationship.Type.PROVIDER_CUSTOMER && !customers.contains(r.to.ASN))
                    if(limit == -1 || customers.size() <= limit)
                        left.add(r.to.ASN);
        }

        return customers;
    }


    /**
     * Checks whether the provided AS lies in the customer cone of another AS.
     * @param root The root of which we will check the customer cone.
     * @param AS The AS that we're trying to find in the customer cone of root.
     * @return true if AS lies in the customer cone of root, false otherwise.
     */
    public boolean inCustomerCone(long root, long AS)
    {
        Set<Long> customers = new HashSet<>();
        Queue<Long> left = new LinkedList<>();
        left.add(root);

        while(!left.isEmpty())
        {
            long X = left.remove();
            customers.add(X);

            for(Relationship r : getRelations(X))
                if((r.type == Relationship.Type.PROVIDER_CUSTOMER || r.type == Relationship.Type.SIBLING_SIBLING) && !customers.contains(r.to.ASN))
                {
                    if(r.to.ASN == AS)
                        return true;
                    else
                        left.add(r.to.ASN);
                }

            // TODO: Change this...
            if(left.size() > 300)
                return false;
        }

        return false;
    }
}

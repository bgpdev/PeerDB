package relation;

import network.AS;

public abstract class Relation
{
    public final AS from;
    public final AS to;

    protected Relation(AS from, AS to)
    {
        this.from = from;
        this.to = to;
    }
}

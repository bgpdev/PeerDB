package relation;

import network.AS;

public class StrictRelation extends Relation
{
    public enum Type
    {
        C2P,
        P2C,
        P2P,
        T2T,
        F2F,
        S2S
    }

    public Type type;

    public StrictRelation(AS from, AS to, Type type)
    {
        super(from, to);
        this.type = type;
    }

    public StrictRelation reverse()
    {
        switch(type)
        {
            case C2P:
                return new StrictRelation(to, from, Type.P2C);
            case P2C:
                return new StrictRelation(to, from, Type.C2P);
            default:
                return new StrictRelation(to, from, type);
        }
    }
}

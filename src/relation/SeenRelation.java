package relation;

import network.AS;
import network.entities.Prefix;

import java.util.*;

public class SeenRelation extends Relation
{
    private final HashMap<Long, Long> seen_by = new HashMap<>();

    public SeenRelation(AS from, AS to)
    {
        super(from, to);
    }

    public void seenBy(Long peer)
    {
        seen_by.putIfAbsent(peer, 0L);
        seen_by.put(peer, seen_by.get(peer) + 1);
    }

    public boolean isSeenBy(Long value)
    {
        return seen_by.containsKey(value);
    }

    public HashMap<Long, Long> isSeenBy()
    {
        return seen_by;
    }

    public long getVisibility(Long[] peers)
    {
        long visible = 0;
        for(Long peer : peers)
            if(isSeenBy(peer))
                visible++;

        return visible;
    }

    public Set<Long> notSeenBy(Long[] peers)
    {
        HashSet<Long> set = new HashSet<>();
        for(Long peer : peers)
            if(!isSeenBy(peer))
                set.add(peer);

        return set;
    }

    public long getVisibility()
    {
        return seen_by.size();
    }
}

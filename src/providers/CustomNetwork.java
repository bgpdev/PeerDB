package providers;

import core.AS;
import core.Relationship;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;

import java.io.*;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.zip.GZIPInputStream;

public class CustomNetwork
{
    public static List<Relationship> getRelationships(Instant time, HashMap<Long, AS> map) throws IOException
    {
        /*
         * Download and create a new ASRank file if it does not already exist.
         */
        LocalDate best = null;
        File asrank = null;

        for (File path : new File("archives/").listFiles())
        {
            if (path.isDirectory())
            {
                LocalDate folder = LocalDate.parse(path.getName());

                File file = new File("archives/" + path.getName() + "/network/network.gz");
                if (file.exists() && (best == null || folder.isAfter(best)))
                {
                    best = folder;
                    asrank = file;
                }
            }
        }

        if(asrank == null)
        {
            System.out.println("[" + new Date().toString() + "][CustomNetwork] No Custom Network file could be found prior to: " + time.toString());
            System.exit(-1);
        }

        System.out.println("[" + new Date().toString() + "][CustomNetwork] Using custom network dump from " + best.toString());
        BufferedReader reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(asrank))));
        List<Relationship> relationships = new ArrayList<>();

        String line = reader.readLine();
        while(line != null)
        {
            if(!line.startsWith("#"))
            {
                String[] split = line.split("\\|");

                long from = Long.parseLong(split[0]);
                long to = Long.parseLong(split[1]);

                if(!map.containsKey(from))
                    map.put(from, new AS(from));
                if(!map.containsKey(to))
                    map.put(to, new AS(to));

                Relationship r = new Relationship(map.get(from),
                        map.get(to),
                        Relationship.Type.fromString(split[2]),
                        "Custom Network");
                relationships.add(r);
            }

            line = reader.readLine();
        }

        System.out.println("[" + new Date().toString() + "][Custom Network] Done.");
        return relationships;
    }
}

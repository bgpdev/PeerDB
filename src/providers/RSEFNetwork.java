package providers;

import core.AS;
import rsef.RSEF;
import rsef.RSEFRecord;

import java.time.Instant;
import java.util.ArrayList;

public class RSEFNetwork
{
    public static ArrayList<AS> getASNs(Instant time) throws Exception
    {
        ArrayList<AS> result = new ArrayList<>();
        for(RSEFRecord record : RSEF.getRecords(time))
        {
            if (record.type.equals("asn") && record.id != null)
            {
                long start = Long.parseLong(record.start);
                for (long i = start; i < start + record.value; i++)
                    result.add(new AS(i, record.id));
            }
        }

        return result;
    }
}

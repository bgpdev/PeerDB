package providers;

import irr.IRR;
import rpsl.entity.AUTNUM;

import java.time.Instant;
import java.util.Calendar;
import java.util.Map;

public class RPSLNetwork
{
    /**
     * Returns a list of AUTNUM objects that contain import and export policies.
     * @param time The date for which we want to retrieve the AUTNUM objects.
     * @return A Map of AUTNUM objects and their ASN number.
     */
    public static Map<Long, AUTNUM> getASN(Instant time)
    {
        IRR irr = new IRR(time);

        return irr.autnums;
    }
}

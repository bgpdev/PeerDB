package providers;

import core.AS;
import core.Relationship;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * The ASRank object takes
 */
public class ASRank
{
    public static List<Relationship> getRelationships(Instant time, HashMap<Long, AS> map) throws IOException
    {
        /*
         * Download and create a new ASRank file if it does not already exist.
         */
        LocalDate best = null;
        File asrank = null;

        for (File path : new File("archives/").listFiles())
        {
            if (path.isDirectory())
            {
                LocalDate folder = LocalDate.parse(path.getName());

                File file = new File("archives/" + path.getName() + "/asrank/as-rel2.bz2");
                if (file.exists() && (best == null || folder.isAfter(best)))
                {
                    best = folder;
                    asrank = file;
                }
            }
        }

        if(asrank == null)
        {
            System.out.println("[" + new Date().toString() + "][ASRank] No ASRank serial2 file could be found prior to: " + time.toString());
            System.exit(-1);
        }

        System.out.println("[" + new Date().toString() + "][ASRank] Using ASRank serial2 dump from " + best.toString());
        BufferedReader reader = new BufferedReader(new InputStreamReader(new BZip2CompressorInputStream(new FileInputStream(asrank))));
        List<Relationship> relationships = new ArrayList<>();

        String line = reader.readLine();
        while(line != null)
        {
            if(!line.startsWith("#"))
            {
                String[] split = line.split("\\|");

                long from = Long.parseLong(split[0]);
                long to = Long.parseLong(split[1]);

                if(!map.containsKey(from))
                    map.put(from, new AS(from));
                if(!map.containsKey(to))
                    map.put(to, new AS(to));

                Relationship r = new Relationship(map.get(from),
                        map.get(to),
                        Relationship.Type.getType(Integer.parseInt(split[2])),
                        "ASRank-Serial2");
                relationships.add(r);
            }

            line = reader.readLine();
        }

        System.out.println("[" + new Date().toString() + "][ASRank] Done.");
        return relationships;
    }

    public static void download(Instant time) throws Exception
    {
        System.out.println("[" + new Date().toString() + "] Checking for ASRank serial2 files..");

        DateTimeFormatter x = DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(ZoneOffset.UTC);
        DateTimeFormatter y = DateTimeFormatter.ofPattern("yyyyMMdd").withZone(ZoneOffset.UTC);

        File file = new File("archives/" + x.format(time) + "/ASRank/as-rel2.txt.bz2");
        if(file.exists())
            return;

        try
        {
            URL url = new URL("http://data.caida.org/datasets/as-relationships/serial-2/" + y.format(time) + ".as-rel2.txt.bz2");
            HttpURLConnection http = (HttpURLConnection)url.openConnection();
            if(http.getResponseCode() != HttpURLConnection.HTTP_OK)
                System.out.println("[" + new Date().toString() + "] ASRank serial2 relations not present for: " + x.format(time));
            else
            {
                System.out.println("[" + new Date().toString() + "] Downloading ASRank serial2 ...");
                FileUtils.copyURLToFile(url, file);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();

            // Delete the file so we can try again on subsequent executions.
            file.delete();

            // Throw a new exception causing the system to reboot.
            throw new Exception("Failed to download the latest ASRank relations.");
        }

        System.out.println("[" + new Date().toString() + "] Done.");
    }
}

package network;

import relation.SeenRelation;
import relation.StrictRelation;

import java.util.*;

public class AS
{
    /** The ASN of this Autonomous System. */
    public final long ASN;

    /** Stores the Relationship with other ASes. */
    public final Map<Long, SeenRelation> seen_relations = new HashMap<>();
    public final Map<Long, StrictRelation> relations = new HashMap<>();

    /** Caches the Customer Cone, this is only calculated when this subtree is satisfied. */
     private boolean satisfied = false;
     public boolean dirty = false;
     private HashSet<Long> customers = new HashSet<>();

    /**
     * Constructs an Autonomous System
     * @param asn The ASN number of the Autonomous System.
     */
    public AS(long asn)
    {
        this.ASN = asn;
        this.customers.add(ASN);
    }

    /**
     * Creates an SeenRelation.
     * @param as The AS to which we should sees.
     */
    public void sees(AS as, Long peer)
    {
        seen_relations.putIfAbsent(as.ASN, new SeenRelation(this, as));
        seen_relations.get(as.ASN).seenBy(peer);
        as.seen_relations.putIfAbsent(ASN, new SeenRelation(as, this));
    }

    /**
     * Creates an SeenRelation.
     * @param as The AS to which we should sees.
     */
    public void connect(AS as, StrictRelation.Type type)
    {
        StrictRelation relation = new StrictRelation(this, as, type);
        relations.putIfAbsent(as.ASN, relation);
        as.relations.putIfAbsent(ASN, relation.reverse());

         // Attempt to satisfy this subtree.
         satisfy();
         as.satisfy();
    }

    /**
     * Creates an SeenRelation.
     * @param as The AS to which we should sees.
     */
    public void connectAndRemove(AS as, StrictRelation.Type type)
    {
        if(as.seen_relations.containsKey(ASN))
            as.seen_relations.remove(ASN);

        if(seen_relations.containsKey(as.ASN))
            seen_relations.remove(as.ASN);

        StrictRelation relation = new StrictRelation(this, as, type);
        relations.putIfAbsent(as.ASN, relation);
        as.relations.putIfAbsent(ASN, relation.reverse());

        // Attempt to satisfy this subtree.
        satisfy();
        as.satisfy();
    }

    /**
     * Returns true if x is definitely inside the Customer Cone, false otherwise.
     * @param x
     * @return
     */
    public boolean inCustomerCone(Long x)
    {
        if(customers.contains(x))
            return true;

        for(Map.Entry<Long, StrictRelation> R : relations.entrySet())
            if(R.getValue().type == StrictRelation.Type.P2C)
                if (!R.getValue().to.isSatisfied() && R.getValue().to.inCustomerCone(x))
                    return true;

        return false;
    }

    /**
     * Checks if this Customer Cone is loop free and not dirty
     */
    public boolean isSafe(Stack<Long> seen)
    {
        if(dirty)
            return false;

        if(seen.contains(ASN))
        {
            System.out.println("Looped Path for " + ASN);
            Collections.reverse(seen);
            while(!seen.empty())
                System.out.print(seen.pop() + ", ");
            System.out.println(ASN);

            return false;
        }

        seen.push(ASN);
        for(Map.Entry<Long, StrictRelation> R : relations.entrySet())
            if(R.getValue().type == StrictRelation.Type.P2C)
                if (!R.getValue().to.isSatisfied() && !R.getValue().to.isSafe(seen))
                    return false;

        seen.pop();
        return true;
    }

    /**
     * Retrieves the Provider relationships of this AS.
     */
    public Set<StrictRelation> getProviders()
    {
        Set<StrictRelation> set = new HashSet<>();
        for(Map.Entry<Long, StrictRelation> x : relations.entrySet())
        {
            if(x.getValue().type == StrictRelation.Type.C2P)
                set.add(x.getValue());
        }

        return set;
    }

    /**
     * Returns true if x is definitely outside the Customer Cone, false otherwise.
     * NOTICE: This only works if one relation X -> Y and Y -!-> X is present.
     * @param x
     * @return
     */
    public boolean outsideCustomerCone(Long x)
    {
        for(Map.Entry<Long, StrictRelation> R : relations.entrySet())
            if(R.getValue().type == StrictRelation.Type.P2C)
                if (!R.getValue().to.isSatisfied())
                    return false;

        return !customers.contains(x);
    }

    public boolean isSatisfied()
    {
        return satisfied;
    }

    private void satisfy()
    {
        if(satisfied)
            return;

        if(!seen_relations.isEmpty())
            return;

        for(Map.Entry<Long, StrictRelation> R : relations.entrySet())
            if(R.getValue().type == StrictRelation.Type.P2C)
                if (!R.getValue().to.isSatisfied())
                    return;

        satisfied = true;
        pushCone();
    }

    private void pushCone()
    {
        for(Map.Entry<Long, StrictRelation> R : relations.entrySet())
            if(R.getValue().type == StrictRelation.Type.C2P)
                R.getValue().to.customers.addAll(customers);

        for(Map.Entry<Long, StrictRelation> R : relations.entrySet())
            if(R.getValue().type == StrictRelation.Type.C2P)
                R.getValue().to.satisfy();
    }
}

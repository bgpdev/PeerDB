package network;

import relation.SeenRelation;
import relation.StrictRelation;

import java.io.*;
import java.util.*;
import java.util.zip.GZIPOutputStream;

public class Topology
{
    /** Mapping of ASN to AS object. */
    public final HashMap<Long, AS> ASes = new HashMap<>();

    public Topology()
    {

    }

    public Topology(InputStream stream) throws IOException
    {
        DataInputStream input = new DataInputStream(stream);

        System.out.println("Loading links...");
        try
        {
            while (true)
            {
                Long ASx = Integer.toUnsignedLong(input.readInt());
                Long ASy = Integer.toUnsignedLong(input.readInt());
                Long ASc = Integer.toUnsignedLong(input.readInt());
                Long amount = Integer.toUnsignedLong(input.readInt());

                ASes.putIfAbsent(ASx, new AS(ASx));
                ASes.putIfAbsent(ASy, new AS(ASy));

                AS a = ASes.get(ASx);
                AS b = ASes.get(ASy);

                // Create a connection.
                a.sees(b, ASc);

                // Set the amount of times this link has been seen.
                a.seen_relations.get(ASy).isSeenBy().put(ASc, amount);
            }
        }
        // GZIP only triggers stream.available = 0 after EOF.
        catch(EOFException e){}
    }

    /**
     * Populates the
     */
    public void populate(InputStream stream) throws IOException
    {
        Scanner scanner = new Scanner(stream);
        System.out.println("Populating Topology...");
        try
        {
            while (scanner.hasNextLine())
            {
                String[] line = scanner.nextLine().split("\\|");

                Long ASx = Long.parseLong(line[0]);
                Long ASy = Long.parseLong(line[1]);
                StrictRelation.Type type = StrictRelation.Type.valueOf(line[2]);

                ASes.putIfAbsent(ASx, new AS(ASx));
                ASes.putIfAbsent(ASy, new AS(ASy));

                AS a = ASes.get(ASx);
                AS b = ASes.get(ASy);

                // Create a connection.
                a.connectAndRemove(b, type);
            }
        }
        // GZIP only triggers stream.available = 0 after EOF.
        catch(Exception e)
        {
            System.out.println("End of file...");
            e.printStackTrace();
        }
    }

    public void toBinaryFile(File file) throws IOException
    {
        DataOutputStream output = new DataOutputStream(new GZIPOutputStream(new FileOutputStream(file)));

        for(Map.Entry<Long, AS> as : ASes.entrySet())
            for(Map.Entry<Long, SeenRelation> relation : as.getValue().seen_relations.entrySet())
            {
                SeenRelation R = relation.getValue();

                for(Map.Entry<Long, Long> x : R.isSeenBy().entrySet())
                {
                    output.writeInt((int)R.from.ASN);
                    output.writeInt((int)R.to.ASN);
                    output.writeInt((int)x.getKey().longValue());
                    output.writeInt((int)x.getValue().longValue());
                }
            }

        output.flush();
        output.close();
    }

    public int getLinksSeen()
    {
        int counter = 0;
        for(Map.Entry<Long, AS> AS : ASes.entrySet())
            counter += AS.getValue().seen_relations.size();

        return counter / 2;
    }

    public void toListing(OutputStream stream) throws IOException
    {
        PrintWriter writer = new PrintWriter(stream);
        for(Map.Entry<Long, AS> AS : ASes.entrySet())
            for(Map.Entry<Long, StrictRelation> x : AS.getValue().relations.entrySet())
            {
                StrictRelation R = x.getValue();
                writer.println(R.from.ASN + "|" + R.to.ASN + "|" + R.type.toString());
            }

        writer.flush();
        writer.close();
    }

    public void toSafeListing(OutputStream stream) throws IOException
    {
        PrintWriter writer = new PrintWriter(stream);
        for(Map.Entry<Long, AS> AS : ASes.entrySet())
            if(AS.getValue().isSafe(new Stack<>()))
                for(Map.Entry<Long, StrictRelation> x : AS.getValue().relations.entrySet())
                {
                    StrictRelation R = x.getValue();
                    writer.println(R.from.ASN + "|" + R.to.ASN + "|" + R.type.toString());
                }

        writer.flush();
        writer.close();
    }

    public void toUltraSafeListing(OutputStream stream) throws IOException
    {
        PrintWriter writer = new PrintWriter(stream);
        for(Map.Entry<Long, AS> AS : ASes.entrySet())
            if(AS.getValue().seen_relations.isEmpty() && AS.getValue().getProviders().size() == 1)
                for(Map.Entry<Long, StrictRelation> x : AS.getValue().relations.entrySet())
                {
                    StrictRelation R = x.getValue();
                    writer.println(R.from.ASN + "|" + R.to.ASN + "|" + R.type.toString());
                }

        writer.flush();
        writer.close();
    }

    /**
     *
     */
    public void printInfo()
    {
        System.out.println("--------------------------------");
        System.out.println("Amount of ASN: " + ASes.size());

        int satisfied = 0;
        int loopfree = 0;
        for(Map.Entry<Long, AS> AS : ASes.entrySet())
        {
            if(AS.getValue().isSatisfied())
                satisfied++;
            if (AS.getValue().isSafe(new Stack<>()))
                loopfree++;
            else
                System.out.println("AS" + AS.getValue().ASN);
        }

        System.out.println("Satisfied ASN: " + satisfied);
        System.out.println("Unsatisfied ASN: " + (ASes.size() - satisfied));
        System.out.println("Safe ASN: " + loopfree);

        int XY = 0;
        int XYYX = 0;
        for(Map.Entry<Long, AS> AS : ASes.entrySet())
            for(Map.Entry<Long, SeenRelation> entry : AS.getValue().seen_relations.entrySet())
            {
                SeenRelation R = entry.getValue();
                if(R.getVisibility() == 0)
                    continue;

                if (R.to.seen_relations.get(R.from.ASN).getVisibility() == 0)
                    XY++;
                else
                    XYYX++;
            }

        System.out.println("X ---> Y relations: " + XY);
        System.out.println("X <--> Y relations: " + (XYYX / 2));
        System.out.println("--------------------------------");
    }
}

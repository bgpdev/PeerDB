package network;

public class Relationship
{
    public final AS from;
    public final AS to;
    public long seen = 0;

    public Relationship(AS from, AS to)
    {
        this.from = from;
        this.to = to;
    }
}

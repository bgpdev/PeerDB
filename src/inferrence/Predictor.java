package inferrence;

import bgp.BGPMessage;
import bgp.BGPReader;
import bgp.BGPUpdate;
import bgp.attributes.AS_PATH;
import bgp.utility.BGPUtility;
import core.PathReader;
import core.PeerUtility;
import mrt.MRTReader;
import mrt.MRTRecord;
import mrt.type.BGP4MP.MESSAGE;
import network.AS;
import network.Topology;
import relation.SeenRelation;
import relation.StrictRelation;
import utility.Pair;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.net.URL;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.zip.GZIPInputStream;

public class Predictor
{
    public static int inferFromLeaves(Topology topology)
    {
        int counter = 0;

        for(Map.Entry<Long, AS> x : topology.ASes.entrySet())
        {
            if(x.getValue().seen_relations.size() == 1)
            {
                boolean C2P = false;
                for(Map.Entry<Long, StrictRelation> relation : x.getValue().relations.entrySet())
                {
                    if(relation.getValue().type == StrictRelation.Type.C2P)
                    {
                        C2P = true;
                        break;
                    }
                }

                if(!C2P)
                {
                    // Create an Iterator so we can access this entry.
                    Iterator<Map.Entry<Long, SeenRelation>> iterator = x.getValue().seen_relations.entrySet().iterator();
                    Map.Entry<Long, SeenRelation> object = iterator.next();

                    SeenRelation R = object.getValue();

                    if(R.getVisibility() == 0)
                        continue;

                    // Remove X -> Y
                    x.getValue().seen_relations.remove(R.to.ASN);

                    // Remove Y -> X
                    R.to.seen_relations.remove(x.getKey());

                    x.getValue().connect(R.to, StrictRelation.Type.C2P);
                    counter++;
                }
            }
        }

        return counter;
    }

    /**
     * Predicts C2P link based on the amount of collectors seeing a link.
     * Theory:
     *  - If X -> Y is seen by V amount of peers and X -!-> Y is not seen, then C2P.
     *  - Unless: All V peers lie in the Customer Cone of Y and no peers lie in Customer Cone of X. (Very Unlikely)
     * @param topology The topology from which the links should be inferred.
     * @param V The visibility threshold that specifies when the inference is applied.
     */
    public static int inferC2PFromVisibility(Topology topology, int V)
    {
        int counter = 0;
        for(Map.Entry<Long, AS> x : topology.ASes.entrySet())
        {
            Iterator<Map.Entry<Long, SeenRelation>> iterator = x.getValue().seen_relations.entrySet().iterator();
            while (iterator.hasNext())
            {
                Map.Entry<Long, SeenRelation> entry = iterator.next();
                SeenRelation R = entry.getValue();

                // If X -> Y is seen by at least V peers.
                if (R.getVisibility() >= V)
                {
                    // If Y -> X is not seen.
                    if (R.to.seen_relations.get(R.from.ASN).getVisibility() == 0)
                    {
                        // Remove X -> Y from seen relationships.
                        iterator.remove();

                        // Remove Y -> X from seen relationships.
                        R.to.seen_relations.remove(R.from.ASN);

                        // Create a P2P relationship.
                        R.from.connect(R.to, StrictRelation.Type.C2P);

                        counter++;
                    }
                }
            }
        }

        return counter;
    }

    /**
     * Predicts P2P link based on the amount of collectors seeing a link.
     * TODO: Specify Theory and Aggregation.
     * Theory:
     * @param topology The topology from which the links should be inferred.
     * @param V The visibility threshold that specifies when the inference is applied.
     */
    public static int inferP2PFromVisibility(Topology topology, int V)
    {
        int counter = 0;
        for(Map.Entry<Long, AS> x : topology.ASes.entrySet())
        {
            Iterator<Map.Entry<Long, SeenRelation>> iterator = x.getValue().seen_relations.entrySet().iterator();
            while(iterator.hasNext())
            {
                Map.Entry<Long, SeenRelation> entry = iterator.next();
                SeenRelation R = entry.getValue();

                // X -> Y must be visible.
                if(R.getVisibility() == 0)
                    continue;

                // If X -> Y is seen by a maximum of V peers.
                if (R.isSeenBy().size() <= V)
                {
                    // If Y -> X is not seen and this is the only relation of Y that has not been verified yet.
                    if (R.to.seen_relations.get(R.from.ASN).getVisibility() == 0)
                    {
                        // If any collector C that saw X -> Y is outside the customer cone of Y, P2P is not possible.
                        // TODO: Fix this, not 100% accurate since there maybe invisible links.
                        boolean P2P = true;
                        for (Map.Entry<Long, Long> peer : R.isSeenBy().entrySet())
                            if (!R.to.isSafe(new Stack<>()) || !R.to.inCustomerCone(peer.getKey()))
                            {
                                P2P = false;
                                break;
                            }

                        if(P2P)
                        {
                            // Remove X -> Y from seen relationships.
                            iterator.remove();

                            // Remove Y -> X from seen relationships.
                            R.to.seen_relations.remove(R.from.ASN);

                            R.from.connect(R.to, StrictRelation.Type.P2P);

                            counter++;
                        }
                    }
                }
            }
        }

        return counter;
    }

    /* ----------------------------------------------------------
     * The FromTopology methods infer new links based on the
     * current topology, they are meant to be 100% accurate.
     * ----------------------------------------------------------*/

    /**
     * Theory:
     * - If X -> Y and Y -!-> X and Collector C that viewed X -> Y lies outside Cone(Y), then C2P.
     * @param topology The topology from which the links should be inferred.
     */
    public static int inferC2PFromTopology_XY(Topology topology)
    {
        int counter = 0;
        for(Map.Entry<Long, AS> AS : topology.ASes.entrySet())
        {
            Iterator<Map.Entry<Long, SeenRelation>> iterator = AS.getValue().seen_relations.entrySet().iterator();

            // Loop over every SeenRelation seen by AS.
            while (iterator.hasNext())
            {
                Map.Entry<Long, SeenRelation> entry = iterator.next();
                SeenRelation R = entry.getValue();

                // Do not process X -!-> Y links.
                if(R.getVisibility() == 0)
                    continue;

                // If Y -> X is not seen and is the only relation of Y.
                if (R.to.seen_relations.get(R.from.ASN).getVisibility() == 0)
                {
                    // If any collector C that saw X -> Y is outside the customer cone of Y, P2P is not possible.
                    for (Map.Entry<Long, Long> peer : R.isSeenBy().entrySet())
                        if (R.to.outsideCustomerCone(peer.getKey()))
                        {
                            // Remove X -> Y from seen relationships.
                            iterator.remove();

                            // Remove Y -> X from seen relationships.
                            R.to.seen_relations.remove(R.from.ASN);

                            // Create a new peer to peer connection.
                            R.from.connect(R.to, StrictRelation.Type.C2P);

                            counter++;
                            break;
                        }
                }
            }
        }

        return counter;
    }

    /**
     * Theory:
     * @param topology The topology from which the links should be inferred.
     */
    public static int inferC2PFromTopology(Topology topology)
    {
        int counter = 0;
        for(Map.Entry<Long, AS> AS : topology.ASes.entrySet())
        {
            Iterator<Map.Entry<Long, SeenRelation>> iterator = AS.getValue().seen_relations.entrySet().iterator();
            while (iterator.hasNext())
            {
                Map.Entry<Long, SeenRelation> entry = iterator.next();
                SeenRelation R = entry.getValue();

                if(R.getVisibility() == 0)
                    continue;

                // If Y -> X is also seen and this is the only relation to Y unknown so far.
                if (R.to.seen_relations.get(R.from.ASN).getVisibility() > 0)
                {
                    // If any collector C that saw X -> Y is outside the customer cone of Y
                    boolean outside = false;
                    for (Map.Entry<Long, Long> peer : R.isSeenBy().entrySet())
                        if (R.to.outsideCustomerCone(peer.getKey()))
                        {
                            outside = true;
                            break;
                        }

                    if(outside)
                    {
                        // If all collectors Cx that saw Y -> X are inside the customer cone of X
                        boolean allIn = true;
                        for (Map.Entry<Long, Long> peer : R.to.seen_relations.get(R.from.ASN).isSeenBy().entrySet())
                            if (!R.from.isSafe(new Stack<>()) || !R.from.inCustomerCone(peer.getKey()))
                            {
                                allIn = false;
                                break;
                            }

                        if(allIn)
                        {
                            // Remove X -> Y
                            iterator.remove();

                            // Remove Y -> X
                            R.to.seen_relations.remove(R.from.ASN);

                            // Insert C2P
                            R.from.connect(R.to, StrictRelation.Type.C2P);

                            counter++;
                        }
                    }
                }
            }
        }

        return counter;
    }

    /**
     * Theory:
     * @param topology The topology from which the links should be inferred.
     */
    public static int inferP2PFromTopology(Topology topology)
    {
        int counter = 0;
        for(Map.Entry<Long, AS> AS : topology.ASes.entrySet())
        {
            Iterator<Map.Entry<Long, SeenRelation>> iterator = AS.getValue().seen_relations.entrySet().iterator();
            while (iterator.hasNext())
            {
                Map.Entry<Long, SeenRelation> entry = iterator.next();
                SeenRelation R = entry.getValue();

                // Skip if X -!-> Y
                if(R.getVisibility() == 0)
                    continue;

                // If Y -> X is also seen
                if (R.to.seen_relations.get(R.from.ASN).getVisibility() > 0)
                {
                    // If any collector C that saw X -> Y is outside the customer cone of Y
                    boolean allIn = true;
                    for (Map.Entry<Long, Long> peer : R.isSeenBy().entrySet())
                    {
                        if (!R.to.isSafe(new Stack<>()) || !R.to.inCustomerCone(peer.getKey()))
                        {
                            allIn = false;
                            break;
                        }
                    }

                    if(allIn)
                    {
                        // If any collector C that saw Y -> X is outside the customer cone of X
                        for (Map.Entry<Long, Long> peer : R.to.seen_relations.get(R.from.ASN).isSeenBy().entrySet())
                        {
                            if (!R.from.isSafe(new Stack<>()) || !R.from.inCustomerCone(peer.getKey()))
                            {
                                allIn = false;
                                break;
                            }
                        }
                    }

                    if(allIn)
                    {
                        // Remove X -> Y
                        iterator.remove();

                        // Remove Y -> X
                        R.to.seen_relations.remove(R.from.ASN);

                        // Insert P2P
                        R.from.connect(R.to, StrictRelation.Type.P2P);

                        counter++;
                    }
                }
            }
        }

        return counter;
    }

    /**
     * Theory:
     * - If X -> Y and Y -> X and X - Y -> C is outside Cone(Y) and Y -> X -> C is outside Cone(X)
     * - If X -> Y and Y -!-> X and Collector C that viewed X -> Y lies outside Cone(Y), then C2P.
     * @param topology The topology from which the links should be inferred.
     */
    public static int inferT2TFromTopology(Topology topology)
    {
        int counter = 0;
        for(Map.Entry<Long, AS> AS : topology.ASes.entrySet())
        {
            Iterator<Map.Entry<Long, SeenRelation>> iterator = AS.getValue().seen_relations.entrySet().iterator();
            while (iterator.hasNext())
            {
                Map.Entry<Long, SeenRelation> entry = iterator.next();
                SeenRelation R = entry.getValue();

                if(R.getVisibility() == 0)
                    continue;

                // If Y -> X is also seen
                if (R.to.seen_relations.get(R.from.ASN).getVisibility() > 0 && R.to.seen_relations.size() == 1 && R.from.seen_relations.size() == 1)
                {
                    // If any collector C that saw X -> Y is outside the customer cone of Y
                    boolean outside = false;
                    for (Map.Entry<Long, Long> peer : R.isSeenBy().entrySet())
                        if (R.to.outsideCustomerCone(peer.getKey()))
                        {
                            outside = true;
                            break;
                        }

                    if(outside)
                        // If any collector C that saw Y -> X is outside the customer cone of X
                        for (Map.Entry<Long, Long> peer : R.to.seen_relations.get(R.from.ASN).isSeenBy().entrySet())
                            if (R.from.outsideCustomerCone(peer.getKey()))
                            {
                                // Remove X -> Y
                                iterator.remove();

                                // Remove Y -> X
                                R.to.seen_relations.remove(R.from.ASN);

                                // Insert T2T
                                R.from.connect(R.to, StrictRelation.Type.T2T);

                                counter++;
                                break;
                            }
                }
            }
        }

        return counter;
    }

    /* ----------------------------------------------------------
     * The FromAS_PATHs
     * Infers paths from the paths seen.
     * ----------------------------------------------------------*/
    public static int inferFromAS_PATH(Topology topology, File directory) throws Exception
    {
        int counter = 0;

        /*
         * Checking RIBs messages.
         */
        for (File file : directory.listFiles())
            if (!file.isDirectory())
            {
                PathReader reader = new PathReader(file);
                AS_PATH path = reader.getNextSanitizedPath();

                while (path != null)
                {
                    counter += inferFromAS_PATH(path, topology);
                    path = reader.getNextSanitizedPath();
                }

                System.out.println("Links inferred so far...: " + counter);
                topology.printInfo();
                System.out.println("-------------------------------------------");
            }

        return counter;
    }

    public static int inferFromAS_PATH(Topology topology, Instant time) throws Exception
    {
        /*
         * Checking Update messages.
         */
        int counter = 0;
        DateTimeFormatter x = DateTimeFormatter.ofPattern("yyyy.MM").withZone(ZoneOffset.UTC);
        DateTimeFormatter y = DateTimeFormatter.ofPattern("yyyyMMdd.HHmm").withZone(ZoneOffset.UTC);

        for(String rrc : BGPUtility.getRRCs())
        {
            for (int i = 0; i < 288; i++)
            {
                String url = "http://data.ris.ripe.net/" + rrc + "/" + x.format(time) + "/updates." + y.format(time) + ".gz";

                DataInputStream stream = new DataInputStream(new GZIPInputStream(new URL(url).openStream()));
                MRTReader reader = new MRTReader(stream);

                try
                {
                    while (true)
                    {
                        MRTRecord entry = reader.read();
                        if (entry instanceof MESSAGE)
                        {
                            MESSAGE m = (MESSAGE) entry;

                            BGPReader.Settings settings = new BGPReader.Settings();
                            settings.EXTENDED_AS4 = true;
                            settings.MRTMode = false;

                            BGPReader bgp = new BGPReader(new DataInputStream(new ByteArrayInputStream(m.message)), settings);

                            try
                            {
                                BGPMessage message = bgp.read();
                                if (message instanceof BGPUpdate)
                                {
                                    BGPUpdate update = (BGPUpdate) message;
                                    BGPUtility.normalize(update);
                                    AS_PATH path = (AS_PATH) update.pathattributes.getOrDefault("AS_PATH", null);

                                    if (path == null)
                                        continue;

                                    // Removes IXP, BGP Prepending and Cisco's AS-Override
                                    BGPUtility.sanitize(path);
                                    counter += inferFromAS_PATH(path, topology);
                                }
                            } catch (Exception e)
                            {
                                System.out.println(e.getMessage());
                                continue;
                            }
                        }
                    }
                }
                catch (EOFException e)
                {
                    stream.close();
                }

                time = time.plus(300, ChronoUnit.SECONDS);
            }
            time = time.minus(1, ChronoUnit.DAYS);

            topology.printInfo();
        }

        return counter;
    }

    private static int inferFromAS_PATH(AS_PATH path, Topology topology)
    {
        int counter = 0;
        List<Pair<Long, Long>> pairs = BGPUtility.getPairs(path);
        Collections.reverse(pairs);

        /*
         * Remove all cases where there are multiple T1's
         */

        if(!PeerUtility.onlyOneT1(pairs))
            return 0;

        boolean P2C_P2P_T2T = false;
        Set<Pair<Long, Long>> left = new HashSet<>();

        for (Pair<Long, Long> pair : pairs)
        {
            AS ASa = topology.ASes.get(pair.getFirst());
            AS ASb = topology.ASes.get(pair.getSecond());

            // If it has already been inferred, check inference.
            if (ASa.relations.containsKey(ASb.ASN))
            {

                if(P2C_P2P_T2T &&
                    ASa.relations.get(ASb.ASN).type == StrictRelation.Type.C2P ||
                    ASa.relations.get(ASb.ASN).type == StrictRelation.Type.P2P)
                {
                    ASa.dirty = true;
                    ASb.dirty = true;
                }


                // If a P2C, P2P or T2T relation has been found, the roof has been found, all after should be P2C.
                if (ASa.relations.get(ASb.ASN).type == StrictRelation.Type.P2C ||
                    ASa.relations.get(ASb.ASN).type == StrictRelation.Type.P2P ||
                    ASa.relations.get(ASb.ASN).type == StrictRelation.Type.T2T)
                    P2C_P2P_T2T = true;

                // If a C2P, P2P or T2T relation has been found, all before should be C2P.
                if (ASa.relations.get(ASb.ASN).type == StrictRelation.Type.C2P ||
                    ASa.relations.get(ASb.ASN).type == StrictRelation.Type.P2P ||
                    ASa.relations.get(ASb.ASN).type == StrictRelation.Type.T2T)
                {
                    for (Pair<Long, Long> x : left)
                    {
                        AS ASx = topology.ASes.get(x.getFirst());
                        AS ASy = topology.ASes.get(x.getSecond());
                        ASx.seen_relations.remove(ASy.ASN);
                        ASy.seen_relations.remove(ASx.ASN);
                        ASx.connect(ASy, StrictRelation.Type.C2P);
                        counter++;
                    }
                    left.clear();
                }

                // When a T1 ASN has been found the X -> Y links before must be C2P and after P2C
                // Place it here such that the first link can also be P2P to a T1.
                if (PeerUtility.isT1(ASa.ASN))
                {
                    P2C_P2P_T2T = true;
                    for (Pair<Long, Long> x : left)
                    {
                        AS ASx = topology.ASes.get(x.getFirst());
                        AS ASy = topology.ASes.get(x.getSecond());
                        ASx.seen_relations.remove(ASy.ASN);
                        ASy.seen_relations.remove(ASx.ASN);
                        ASx.connect(ASy, StrictRelation.Type.C2P);
                        counter++;
                    }
                    left.clear();
                }
            }

            // Y -> X is not seen.
            else if (ASb.seen_relations.get(ASa.ASN).getVisibility() == 0)
            {
                // If the roof has already been seen. (When left to right, right side of tree)
                if (P2C_P2P_T2T)
                {
                    ASa.seen_relations.remove(ASb.ASN);
                    ASb.seen_relations.remove(ASa.ASN);
                    ASa.connect(ASb, StrictRelation.Type.P2C);
                    counter++;
                }

                // If the roof has not been seen. (When left to right, left side of tree)
                else
                    left.add(pair);
            }
        }

        return counter;
    }
}

package inferrence;

import bgp.utility.BGPUtility;
import core.PeerUtility;
import network.AS;
import network.Topology;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import relation.SeenRelation;
import relation.StrictRelation;

import java.io.*;
import java.time.Instant;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class Launcher
{
    public static void main(String[] args) throws Exception
    {
        Instant time = Instant.parse("2018-06-01T00:00:00.00Z");

        File ribs = new File("../PeerDB/ribs/");
        File links = new File("links.gz");
        File population = new File("population.gz");
        File safe = new File("safe.gz");
        File ultra_safe = new File("ultra_safe.gz");
        File listing = new File("listing.gz");
        File asrank = new File("archives/2018-06-01/asrank/as-rel2.bz2");

        // 1. Sanitize the routes and generate a file with all the links.
        if(!links.exists())
        {
            System.out.println("Sanetizing file...");
            Sanitizer sanitizer = new Sanitizer();
            // sanitizer.construct(time);
            sanitizer.construct(ribs);
            sanitizer.getTopology().toBinaryFile(links);
        }

        // 2. Infer relationships
        Topology topology = new Topology(new GZIPInputStream(new FileInputStream(links)));
        System.out.println("Links Seen: " + topology.getLinksSeen());
        // topology.populate(new GZIPInputStream(new FileInputStream(population)));
        topology.populate(new FileInputStream(new File("manual/AS15412.lst")));
        topology.populate(new FileInputStream(new File("manual/manual.lst")));
        topology.printInfo();

        // --------------------------------------------------------------------------------
        // --------------------------------------------------------------------------------

        // Initialize the T2T relations.
        for(Long a : PeerUtility.T1)
            for(Long b : PeerUtility.T1)
            {
                AS ASa = topology.ASes.get(a);
                AS ASb = topology.ASes.get(b);
                if(ASa.seen_relations.containsKey(b))
                {
                    ASa.seen_relations.remove(b);
                    ASb.seen_relations.remove(a);
                    ASa.connect(ASb, StrictRelation.Type.T2T);
                }
            }

        // --------------------------------------------------------------------------------
        // --------------------------------------------------------------------------------

        {
            int total = -1;
            while (total != 0)
            {
                total = 0;
//                {
//                    int counter = Predictor.inferP2PFromVisibility(topology, 300);
//                    total += counter;
//                    System.out.println("Links inferred from P2P Visibility: " + counter);
//                    topology.printInfo();
//                    System.out.println("-------------------------------------------");
//                }
//                {
//                    int counter = Predictor.inferC2PFromTopology_XY(topology);
//                    total += counter;
//                    System.out.println("Links inferred from inferC2PFromTopology_XY: " + counter);
//                    topology.printInfo();
//                    System.out.println("-------------------------------------------");
//                }
//                {
//                    int counter = Predictor.inferP2PFromTopology(topology);
//                    total += counter;
//                    System.out.println("Links inferred from inferP2PFromTopology: " + counter);
//                    topology.printInfo();
//                    System.out.println("-------------------------------------------");
//                }
//                {
//                    int counter = Predictor.inferC2PFromTopology(topology);
//                    total += counter;
//                    System.out.println("Links inferred from inferC2PFromTopology: " + counter);
//                    topology.printInfo();
//                    System.out.println("-------------------------------------------");
//                }
//                {
//                    int counter = Predictor.inferFromAS_PATH(topology, time);
//                    total += counter;
//                    System.out.println("Links inferred from AS_PATHs: " + counter);
//                    topology.printInfo();
//                    System.out.println("-------------------------------------------");
//                }

            }
        }

        {
            int counter = Predictor.inferFromAS_PATH(topology, ribs);
            // total += counter;
            System.out.println("Links inferred from AS_PATHs: " + counter);
            topology.printInfo();
            System.out.println("-------------------------------------------");
        }

        {
            int counter = Predictor.inferFromAS_PATH(topology, ribs);
            // total += counter;
            System.out.println("Links inferred from AS_PATHs: " + counter);
            topology.printInfo();
            System.out.println("-------------------------------------------");
        }

        // 3. Write the relationships to a file.
        if(listing.exists())
            listing.delete();

        if(safe.exists())
            safe.delete();

        if(ultra_safe.exists())
            ultra_safe.delete();

        topology.toListing(new GZIPOutputStream(new FileOutputStream(listing)));
        topology.toSafeListing(new GZIPOutputStream(new FileOutputStream(safe)));
        topology.toUltraSafeListing(new GZIPOutputStream(new FileOutputStream(ultra_safe)));

        compare(new GZIPInputStream(new FileInputStream(listing)), new BZip2CompressorInputStream(new FileInputStream(asrank)));
    }

    /**
     * Compares the new listing with ASRank.
     * @param listing
     * @param asrank
     * @throws IOException
     */
    public static void compare(InputStream listing, InputStream asrank) throws IOException
    {
        HashSet<String> asrank_set = new HashSet<>();

        long missing = 0;
        long wrong = 0;

        {
            Scanner scanner = new Scanner(asrank);

            while (scanner.hasNextLine())
            {
                String x = scanner.nextLine();
                if(x.contains("#"))
                    continue;

                String[] split = x.split("\\|");

                Long ASa = Long.parseLong(split[0]);
                Long ASb = Long.parseLong(split[1]);
                Long type = Long.parseLong(split[2]);

                if(type.equals(-1L))
                {
                    asrank_set.add(ASa + "|" + ASb + "|P2C");
                    asrank_set.add(ASb + "|" + ASa + "|C2P");
                }
                if(type.equals(0L))
                {
                    asrank_set.add(ASa + "|" + ASb + "|P2P");
                    asrank_set.add(ASb + "|" + ASa + "|P2P");
                }
            }
        }

        {
            Scanner scanner = new Scanner(listing);

            while (scanner.hasNextLine())
            {
                String x = scanner.nextLine();
                String[] split = x.split("\\|");

                Long ASa = Long.parseLong(split[0]);
                Long ASb = Long.parseLong(split[1]);
                String type = split[2];

                String key = ASa + "|" + ASb + "|" + type;

                if(asrank_set.contains(ASa + "|" + ASb + "|P2P") || asrank_set.contains(ASa + "|" + ASb + "|C2P") || asrank_set.contains(ASa + "|" + ASb + "|P2C"))
                {
                    if(!asrank_set.contains(key))
                    {
                        // System.out.println("Different Link: " + ASa + " -> " + ASb);
                        wrong++;
                    }
                }
                else
                {
                    // System.out.println("Link not present: " + ASa + " -> " + ASb);
                    missing++;
                }
            }
        }

        System.out.println("Missing Links: " + missing);
        System.out.println("Wrong Links: " + wrong);
    }
}

package inferrence;

import bgp.BGPMessage;
import bgp.BGPReader;
import bgp.BGPUpdate;
import bgp.PathAttribute;
import bgp.attributes.AS_PATH;
import bgp.utility.BGPUtility;
import mrt.MRTReader;
import mrt.MRTRecord;
import mrt.type.BGP4MP.MESSAGE;
import mrt.type.TABLE_DUMP;
import mrt.type.TABLE_DUMP_V2.RIB_AFI;
import network.AS;
import network.Topology;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import utility.Pair;

import java.io.*;
import java.net.URL;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Map;
import java.util.zip.GZIPInputStream;

public class Sanitizer
{
    private Topology topology = new Topology();

    /**
     * Constructs the Topology.
     * @param directory
     * @throws Exception
     */
    public void construct(File directory) throws Exception
    {
        for (File file : directory.listFiles())
            if (!file.isDirectory())
                collect(file);
    }

    /**
     * Constructs the Topology from updates.
     * @throws Exception
     */
    public void construct(Instant time) throws Exception
    {
        DateTimeFormatter x = DateTimeFormatter.ofPattern("yyyy.MM").withZone(ZoneOffset.UTC);
        DateTimeFormatter y = DateTimeFormatter.ofPattern("yyyyMMdd.HHmm").withZone(ZoneOffset.UTC);

        for(String rrc : BGPUtility.getRRCs())
        {
            for (int i = 0; i < 288; i++)
            {
                String url = "http://data.ris.ripe.net/" + rrc + "/" + x.format(time) + "/updates." + y.format(time) + ".gz";
                collect(url);
                time = time.plus(300, ChronoUnit.SECONDS);
            }
            time = time.minus(1, ChronoUnit.DAYS);

            topology.printInfo();
        }
    }

    /**
     * Collects all information from the RIB.
     * @param file
     * @throws Exception
     */
    private void collect(File file) throws Exception
    {
        System.out.println("Scraping: " + file.getName());

        DataInputStream stream;
        if(file.getName().endsWith(".gz"))
            stream = new DataInputStream(new GZIPInputStream(new FileInputStream(file)));
        else if(file.getName().endsWith(".bz2"))
            stream = new DataInputStream(new BZip2CompressorInputStream(new FileInputStream(file)));
        else
            stream = new DataInputStream(new FileInputStream(file));

        long counter = 0;
        try
        {
            MRTReader reader = new MRTReader(stream);
            while (true)
            {
                MRTRecord entry = reader.read();

                if (entry instanceof RIB_AFI)
                {
                    RIB_AFI a = (RIB_AFI) entry;
                    for(RIB_AFI.RIB_ENTRY x : a.entries)
                    {
                        AS_PATH path = BGPUtility.getAS_PATH(new DataInputStream(new ByteArrayInputStream(x.attributes)));

                        // Removes IXP, BGP Prepending and Cisco's AS-Override
                        BGPUtility.sanitize(path);

                        // Adds the relations to the mesh.
                        process(path, Integer.toUnsignedLong(x.peer.ASN));

                        if(counter++ % 100000 == 0)
                            System.out.println("Counter: " + counter);
                    }
                }


                if(entry instanceof TABLE_DUMP)
                {
                    System.out.println("Test");
                    TABLE_DUMP a = (TABLE_DUMP)entry;
                    AS_PATH path = BGPUtility.getAS_PATH(new DataInputStream(new ByteArrayInputStream(a.attributes)));

                    // Removes IXP, BGP Prepending and Cisco's AS-Override
                    BGPUtility.sanitize(path);

                    // Adds the relations to the mesh.
                    process(path, Integer.toUnsignedLong(a.ASN));

                    if(counter++ % 100000 == 0)
                        System.out.println("Counter: " + counter);
                }
            }
        } catch (EOFException e)
        {
            stream.close();
        }

        // Close the stream to free resources.
        stream.close();
    }

    private void collect(String url) throws Exception
    {
        System.out.println("Scraping: " + url);

        DataInputStream stream;
        if(url.endsWith(".gz"))
            stream = new DataInputStream(new GZIPInputStream(new URL(url).openStream()));
        else if(url.endsWith(".bz2"))
            stream = new DataInputStream(new BZip2CompressorInputStream(new URL(url).openStream()));
        else
            stream = new DataInputStream(new URL(url).openStream());

        long counter = 0;
        try
        {
            MRTReader reader = new MRTReader(stream);
            while (true)
            {
                MRTRecord entry = reader.read();

                if (entry instanceof RIB_AFI)
                {
                    RIB_AFI a = (RIB_AFI) entry;
                    for(RIB_AFI.RIB_ENTRY x : a.entries)
                    {
                        AS_PATH path = BGPUtility.getAS_PATH(new DataInputStream(new ByteArrayInputStream(x.attributes)));

                        // Removes IXP, BGP Prepending and Cisco's AS-Override
                        BGPUtility.sanitize(path);

                        // Adds the relations to the mesh.
                        process(path, Integer.toUnsignedLong(x.peer.ASN));
                    }
                }

                if(entry instanceof MESSAGE)
                {
                    MESSAGE m = (MESSAGE) entry;

                    BGPReader.Settings settings = new BGPReader.Settings();
                    settings.EXTENDED_AS4 = true;
                    settings.MRTMode = false;

                    BGPReader bgp = new BGPReader(new DataInputStream(new ByteArrayInputStream(m.message)), settings);

                    try
                    {
                        BGPMessage message = bgp.read();
                        if(message instanceof BGPUpdate)
                        {
                            BGPUpdate update = (BGPUpdate) message;
                            BGPUtility.normalize(update);
                            AS_PATH path = (AS_PATH) update.pathattributes.getOrDefault("AS_PATH", null);

                            if(path == null)
                                continue;

                            // Removes IXP, BGP Prepending and Cisco's AS-Override
                            BGPUtility.sanitize(path);

                            // Adds the relations to the mesh.
                            process(path, Integer.toUnsignedLong(m.peerASN));
                        }
                    }
                    catch (Exception e)
                    {
                        System.out.println(e.getMessage());
                        continue;
                    }
                }

                if(entry instanceof TABLE_DUMP)
                {
                    System.out.println("Test");
                    TABLE_DUMP a = (TABLE_DUMP)entry;
                    AS_PATH path = BGPUtility.getAS_PATH(new DataInputStream(new ByteArrayInputStream(a.attributes)));

                    // Removes IXP, BGP Prepending and Cisco's AS-Override
                    BGPUtility.sanitize(path);

                    // Adds the relations to the mesh.
                    process(path, Integer.toUnsignedLong(a.ASN));
                }
            }
        } catch (EOFException e)
        {
            stream.close();
        }

        // Close the stream to free resources.
        stream.close();
    }

    /**
     * Construct the AS Topology and add relations where necessary.
     * @param path The AS_PATH to convert to relationships
     * @param peer The peer by which the AS_PATH has been seen.
     */
    private void process(AS_PATH path, Long peer)
    {
        // Create Relationships in the Mesh
        for(Pair<Long, Long> pair : BGPUtility.getPairs(path))
        {
            topology.ASes.putIfAbsent(pair.getFirst(), new AS(pair.getFirst()));
            topology.ASes.putIfAbsent(pair.getSecond(), new AS(pair.getSecond()));

            AS a = topology.ASes.get(pair.getFirst());
            AS b = topology.ASes.get(pair.getSecond());

            a.sees(b, peer);
        }
    }

    public Topology getTopology()
    {
        return topology;
    }
}
